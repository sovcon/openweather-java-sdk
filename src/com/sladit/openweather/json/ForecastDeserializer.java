package com.sladit.openweather.json;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.sladit.openweather.json.forecast.Forecast;
import com.sladit.openweather.json.forecast.ForecastDay;

public class ForecastDeserializer implements JsonDeserializer<Forecast> {

	@Override
	public Forecast deserialize(JsonElement json, Type type,
			JsonDeserializationContext context) throws JsonParseException {
		
		Forecast response = new Forecast();
		List<ForecastDay> days = new ArrayList<ForecastDay>();
		
		JsonObject object = json.getAsJsonObject();
		
		for (Map.Entry<String,JsonElement> entry : object.entrySet()) {
			try {
			    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			    Date date = format.parse(entry.getKey());
			    
			    GsonBuilder gsonBuilder = new GsonBuilder();
				gsonBuilder.registerTypeAdapter(Timestamp.class, new TimestampDeserializer());
				gsonBuilder.registerTypeAdapter(ForecastDay.class, new ForecastDayDeserializer());
			    Gson gson = gsonBuilder.create();
			    ForecastDay day = gson.fromJson(entry.getValue(), ForecastDay.class);
			    
			    day.setDate(date);
			    
			    days.add(day);
			    
			} catch(ParseException e) {
				
			}
		}
		
		response.setDays(days);
		
		return response;
	}

}
