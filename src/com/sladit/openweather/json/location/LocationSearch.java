package com.sladit.openweather.json.location;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sladit.openweather.json.WetterCredit;

/**
 * This class holds location search information and results.
 * 
 * @author Leonid
 */
public class LocationSearch {

	@SerializedName("search_string")
	@Expose
	private String searchString;
	
	@Expose
	private Integer maxhits;
	
	@Expose
	private Integer hits;
	
	@SerializedName("exact_match")
	@Expose
	private Boolean exactMatch;
	@Expose
	private WetterCredit credit;
	
	@SerializedName("result")
	@Expose
	private List<LocationResult> results = new ArrayList<LocationResult>();

	/**
	 * @return The original search String
	 */
	public String getSearchString() {
		return searchString;
	}

	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	/**
	 * @return amount of maximum search hits
	 */
	public Integer getMaxhits() {
		return maxhits;
	}

	public void setMaxhits(Integer maxhits) {
		this.maxhits = maxhits;
	}

	/**
	 * @return amount of hits for this search
	 */
	public Integer getHits() {
		return hits;
	}

	public void setHits(Integer hits) {
		this.hits = hits;
	}

	/**
	 * @return true if search was an exact match
	 */
	public Boolean getExactMatch() {
		return exactMatch;
	}

	public void setExactMatch(Boolean exactMatch) {
		this.exactMatch = exactMatch;
	}

	/**
	 * @return wetter.com openweather API creadit information
	 */
	public WetterCredit getCredit() {
		return credit;
	}

	public void setCredit(WetterCredit credit) {
		this.credit = credit;
	}

	/**
	 * Get results
	 */
	public List<LocationResult> getResults() {
		return results;
	}

	public void setResults(List<LocationResult> result) {
		this.results = result;
	}

}