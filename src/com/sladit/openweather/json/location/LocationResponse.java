package com.sladit.openweather.json.location;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * JSON wrapper class for for a location response.
 * 
 * @author Leonid Koftun
 *
 */
public class LocationResponse {

	@Expose
	private LocationSearch search;

	/**
	 * Get the search object for this response
	 * @return
	 */
	public LocationSearch getSearch() {
		return search;
	}

	public void setSearch(LocationSearch search) {
		this.search = search;
	}
	
	/**
	 * Shortcut to {@link LocationSearch#getResults()} at {@link #getSearch()}
	 */
	public List<LocationResult> getResults() {
		return this.getSearch() != null ? this.getSearch().getResults() : new ArrayList<>(); 
	}

}