package com.sladit.openweather.json.location;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sladit.openweather.OpenweatherConnection;

/**
 * This model holds location result information.
 * 
 * @author Leonid Koftun
 */
public class LocationResult {

	@SerializedName("city_code")
	@Expose
	private String cityCode;
	
	@Expose
	private String plz;
	
	@Expose
	private String name;
	
	@Expose
	private String quarter;
	
	@SerializedName("adm_1_code")
	@Expose
	private String countryCode;
	
	@SerializedName("adm_1_name")
	@Expose
	private String countryName;
	
	@SerializedName("adm_2_name")
	@Expose
	private String stateName;
	
	@SerializedName("adm_4_name")
	@Expose
	private String regionName;

	/**
	 * Get the city code required for {@link OpenweatherConnection#queryForecast(String)}
	 * @return The cityCode
	 */
	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	/**
	 * @return postal index 
	 * TODO Rename method name
	 */
	public String getPlz() {
		return plz;
	}

	public void setPlz(String plz) {
		this.plz = plz;
	}

	/**
	 * @return name
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return quarter name
	 */
	public String getQuarter() {
		return quarter;
	}

	public void setQuarter(String quarter) {
		this.quarter = quarter;
	}

	/**
	 * @return country code
	 */
	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	/**
	 * @return country name
	 */
	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * @return state name
	 */
	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return region name
	 */
	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	
}