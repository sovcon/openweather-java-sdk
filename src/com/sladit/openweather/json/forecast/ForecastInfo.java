package com.sladit.openweather.json.forecast;

import java.sql.Timestamp;

import com.google.gson.annotations.SerializedName;

/**
 * This class stores actual weather forecast information for a point in time (
 * {@link ForecastDay}, {@link ForecastTime}).
 * 
 * @author Leonid Koftun
 *
 */
public abstract class ForecastInfo {

	@SerializedName("d")
	private Timestamp timestampLocal;
	@SerializedName("du")
	private Timestamp timestampUTC;

	@SerializedName("wd")
	private Integer windDirection;
	@SerializedName("wd_txt")
	private String windDirectionText;

	@SerializedName("w")
	private Integer weatherStatus;
	@SerializedName("w_txt")
	private String weatherStatusText;

	@SerializedName("tn")
	private Integer minTemp;
	@SerializedName("tx")
	private Integer maxTemp;

	@SerializedName("pc")
	private Integer pressureChange;

	@SerializedName("p")
	private Integer validityPeriod;

	@SerializedName("ws")
	private Double windSpeed;

	public Timestamp getTimestampLocal() {
		return timestampLocal;
	}

	public void setTimestampLocal(Timestamp timestampLocal) {
		this.timestampLocal = timestampLocal;
	}

	public Timestamp getTimestampUTC() {
		return timestampUTC;
	}

	public void setTimestampUTC(Timestamp timestampUTC) {
		this.timestampUTC = timestampUTC;
	}

	/**
	 * Get raw wind direction
	 * 
	 * @return wind direction in DEG
	 */
	public Integer getWindDirection() {
		return windDirection;
	}

	public void setWindDirection(Integer windDirection) {
		this.windDirection = windDirection;
	}

	/**
	 * Get human readable wind direction
	 * 
	 * @return String
	 */
	public String getWindDirectionText() {
		return windDirectionText;
	}

	public void setWindDirectionText(String windDirectionText) {
		this.windDirectionText = windDirectionText;
	}

	/**
	 * Get raw weather status codes.
	 * 
	 * @see <a
	 *      href="http://www.wetter.com/apps_und_mehr/website/api/dokumentation/">API
	 *      Documentation (German)</a>
	 * @return weather status code
	 */
	public Integer getWeatherStatus() {
		return weatherStatus;
	}

	public void setWeatherStatus(Integer weatherStatus) {
		this.weatherStatus = weatherStatus;
	}

	/**
	 * Get human readable weather status
	 * 
	 * @return String weather status description (e.g. "cloudy", "sunny")
	 */
	public String getWeatherStatusText() {
		return weatherStatusText;
	}

	public void setWeatherStatusText(String weatherStatusText) {
		this.weatherStatusText = weatherStatusText;
	}

	/**
	 * Get minimum temperature
	 * 
	 * @return temperature in Celsius
	 */
	public Integer getMinTemp() {
		return minTemp;
	}

	public void setMinTemp(Integer minTemp) {
		this.minTemp = minTemp;
	}

	/**
	 * Get maximum temperature
	 * 
	 * @return temperature in Celsius
	 */
	public Integer getMaxTemp() {
		return maxTemp;
	}

	public void setMaxTemp(Integer maxTemp) {
		this.maxTemp = maxTemp;
	}

	/*
	 * TODO find out how to interpret this data
	 */
	public Integer getPressureChange() {
		return pressureChange;
	}

	public void setPressureChange(Integer pressureChange) {
		this.pressureChange = pressureChange;
	}

	/**
	 * Get validity period
	 * 
	 * @return amount of hours for which this forecast information will be valid
	 *         for
	 */
	public Integer getValidityPeriod() {
		return validityPeriod;
	}

	public void setValidityPeriod(Integer validityPeriod) {
		this.validityPeriod = validityPeriod;
	}

	/**
	 * Get wind speed
	 * @return wind speed in km/h
	 */
	public Double getWindSpeed() {
		return windSpeed;
	}

	public void setWindSpeed(Double windSpeed) {
		this.windSpeed = windSpeed;
	}

}
