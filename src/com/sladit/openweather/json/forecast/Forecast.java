package com.sladit.openweather.json.forecast;

import java.util.List;

/**
 * The Forecast model contains a list of {@link ForecastDay}.
 * @author Leonid Koftun
 *
 */
public class Forecast {

	private List<ForecastDay> days;

	public List<ForecastDay> getDays() {
		return days;
	}

	public void setDays(List<ForecastDay> days) {
		this.days = days;
	}

}
