package com.sladit.openweather.json.forecast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.sladit.openweather.json.WetterCredit;

/**
 * Openweather City Model contains location and forecast data.
 * @author Leonid Koftun
 *
 */
public class City {
	
	@SerializedName("city_code")
	@Expose
	private String cityCode;
	
	@SerializedName("name")
	@Expose
	private String cityName;
	
	@Expose
	private String url;
	
	@Expose
	private WetterCredit credit;

	@Expose
	private Forecast forecast;
	
	public String getCityCode() {
		return cityCode;
	}

	public void setCityCode(String cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public WetterCredit getCredit() {
		return credit;
	}

	public void setCredit(WetterCredit credit) {
		this.credit = credit;
	}

	public Forecast getForecast() {
		return forecast;
	}

	public void setForecast(Forecast forecasts) {
		this.forecast = forecasts;
	}
}
