package com.sladit.openweather.json.forecast;


/**
 * This model gives you access to the forecast for a date at a specific time by extending {@link ForecastInfo}.
 * 
 * @author Leonid Koftun
 *
 */
public class ForecastTime extends ForecastInfo {
	
}
