package com.sladit.openweather.json.forecast;

import java.util.Date;
import java.util.Map;

/**
 * The ForecastDay model gives you access to the daily average forecast for a
 * date by extending {@link ForecastInfo}. Use {@link #getForecastTimes()} to
 * access weather forecast details for several predefined times for this date.
 * 
 * @author Leonid Koftun
 *
 */
public class ForecastDay extends ForecastInfo {

	private Date date;

	private Map<String, ForecastTime> forecastTimes;

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	/**
	 * Get weather forecast details for several times on this day. The times are
	 * fixed and predefined by the openweather API. Right now these times are
	 * set to
	 * 
	 * <ul>
	 * <li>06:00 (morning)</li>
	 * <li>11:00 (noon)</li>
	 * <li>17:00 (evening)</li>
	 * <li>23:00 (night)</li>
	 * </ul>
	 * 
	 * These times could change in the future according to the API
	 * documentation.
	 * 
	 * @return Map where key = Time String (e.g. "17:00"), value =
	 *         {@link ForecastTime}
	 */
	public Map<String, ForecastTime> getForecastTimes() {
		return forecastTimes;
	}

	public void setForecastTimes(Map<String, ForecastTime> forecastTimes) {
		this.forecastTimes = forecastTimes;
	}

}
