package com.sladit.openweather.json.forecast;

import com.google.gson.annotations.Expose;

/**
 * JSON wrapper class for for a forecast response.
 * 
 * @author Leonid Koftun
 *
 */
public class ForecastResponse {

	@Expose
	private City city;

	public City getCity() {
		return city;
	}

	public void setCity(City city) {
		this.city = city;
	}

}
