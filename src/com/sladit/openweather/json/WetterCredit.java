package com.sladit.openweather.json;

import com.google.gson.annotations.Expose;

/**
 * Use of the wetter.com openweather API requires you to give credit. The credit
 * info can be accessed over this model.
 * 
 * @author Leonid Koftun
 *
 */
public class WetterCredit {

	@Expose
	private String info;
	@Expose
	private String text;
	@Expose
	private String link;
	@Expose
	private String logo;

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	/**
	 * @deprecated Not implemented yet
	 */
	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

}