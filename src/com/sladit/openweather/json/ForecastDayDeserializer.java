package com.sladit.openweather.json;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.sladit.openweather.json.forecast.ForecastDay;
import com.sladit.openweather.json.forecast.ForecastTime;

public class ForecastDayDeserializer implements JsonDeserializer<ForecastDay> {

	@Override
	public ForecastDay deserialize(JsonElement json, Type type,
			JsonDeserializationContext context) throws JsonParseException {
		JsonObject object = json.getAsJsonObject();

		Map<String, ForecastTime> times = new TreeMap<>();
		
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Timestamp.class, new TimestampDeserializer());
	    Gson gson = gsonBuilder.create();
		
	    ForecastDay day = gson.fromJson(json, ForecastDay.class);
	    
	    String pattern = "(\\d{2}:\\d{2})"; // Might be a bit naive...
	    
		for (Map.Entry<String,JsonElement> entry : object.entrySet()) {
			try {
				
				if (entry.getKey().matches(pattern)) {
					ForecastTime time = gson.fromJson(entry.getValue(), ForecastTime.class);
					times.put(entry.getKey(), time);
				}
			    
			} catch(Exception e) {
				// TODO Auto generated catch block
				System.out.println("Exception in ForecastDayDeserializer");
			}
		}
		
		day.setForecastTimes(times);
		
		return day;
	}

}
