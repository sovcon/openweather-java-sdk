package com.sladit.openweather.json;

import java.lang.reflect.Type;
import java.sql.Timestamp;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class TimestampDeserializer implements JsonDeserializer<Timestamp>{

	@Override
	public Timestamp deserialize(JsonElement json, Type type,
			JsonDeserializationContext context) throws JsonParseException {
		long millis = json.getAsLong() * 1000;
		Timestamp timestamp = new Timestamp(millis);
	    return timestamp;
	}

}
