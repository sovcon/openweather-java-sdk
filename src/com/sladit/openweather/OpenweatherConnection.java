package com.sladit.openweather;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sladit.openweather.json.ForecastDeserializer;
import com.sladit.openweather.json.forecast.Forecast;
import com.sladit.openweather.json.forecast.ForecastResponse;
import com.sladit.openweather.json.location.LocationResponse;

/**
 * Represents a connection to the wetter.com openweather API. To receive data
 * from their API, you must supply the connection with an {@link Authenticator}
 * instance. You can then query the API for locations and their corresponding
 * forecast data.
 * 
 * @author Leonid Koftun
 * @see <a href="https://bitbucket.org/sovcon/openweather-java-sdk">Git Repository</a>
 * @see <a href="http://www.wetter.com/apps_und_mehr/website/api/dokumentation/">openweather API docs (German)</a>
 * @see <a href="http://openweather.sladit.com">Live Spring Demo</a>
 * 
 */
public class OpenweatherConnection {

	public static final String API_BASE_URL = "http://api.wetter.com";

	private Authenticator authenticator;
	private String languageIdentifier = "en";

	/**
	 * Create new Openweather API Connection from Authenticator
	 * 
	 * @param authenticator
	 */
	public OpenweatherConnection(Authenticator authenticator) {
		this.authenticator = authenticator;
	}

	/**
	 * Create new Openweather API Connection from authentication details
	 * 
	 * @param projectName
	 * @param apiKey
	 */
	public OpenweatherConnection(String projectName, String apiKey) {
		this.authenticator = new Authenticator(projectName, apiKey);
	}

	/**
	 * Create new Openweather API Connection from Authenticator and set output
	 * language
	 * 
	 * @param authenticator
	 * @param language
	 */
	public OpenweatherConnection(Authenticator authenticator, String language) {
		this.authenticator = authenticator;
		setOutputLangauge(language);
	}

	/**
	 * Create new Openweather API Connection from authentication details and set
	 * output language
	 * 
	 * @param projectName
	 * @param apiKey
	 * @param language
	 */
	public OpenweatherConnection(String projectName, String apiKey, String language) {
		this.authenticator = new Authenticator(projectName, apiKey);
		setOutputLangauge(language);
	}

	/**
	 * Sets api output language.
	 * 
	 * @param languageIdentifier
	 *            (en, de, es)
	 */
	public void setOutputLangauge(String languageIdentifier) {
		this.languageIdentifier = languageIdentifier;
	}

	/**
	 * Find a specific location
	 * 
	 * @param location
	 *            search query
	 * @return {@link LocationResponse}
	 * @throws IOException
	 */
	public LocationResponse queryLocation(String location) throws IOException {

		URL url = makeApiUrl("/location/index/search/", location);

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		InputStream in = connection.getInputStream();
		String response = convertStreamToString(in);
		in.close();

		Gson gson = new Gson();
		LocationResponse locationResponse = gson.fromJson(response,
				LocationResponse.class);

		return locationResponse;
	}

	/**
	 * Get weather forecast for a specified cityCode
	 * 
	 * @param cityCode
	 *            acquired from LocationResponse
	 * @return {@link ForecastResponse}
	 * @throws IOException
	 */
	public ForecastResponse queryForecast(String cityCode) throws IOException {

		URL url = makeApiUrl("/forecast/weather/city/", cityCode);

		HttpURLConnection connection = (HttpURLConnection) url.openConnection();

		InputStream in = connection.getInputStream();
		String response = convertStreamToString(in);
		in.close();

		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.registerTypeAdapter(Forecast.class, new ForecastDeserializer());
		Gson gson = gsonBuilder.create();

		ForecastResponse forecastResponse = gson.fromJson(response,
				ForecastResponse.class);

		return forecastResponse;
	}

	/**
	 * Creates a url encoded URL from parameters
	 * 
	 * @param path
	 *            The API path
	 * @param variable
	 *            The variable to send
	 * @return URLEncoded URL
	 */
	private URL makeApiUrl(String path, String variable) {

		String encodedVariable;
		URL url = null;

		try {
			encodedVariable = URLEncoder.encode(variable, "UTF-8");

			String urlString = API_BASE_URL + path + encodedVariable + "/project/"
					+ authenticator.getProjectName() + "/cs/"
					+ authenticator.getChecksum(variable) + "/output/json" + "/language/"
					+ languageIdentifier;

			// System.out.println(urlString);

			url = new URL(urlString);
		} catch (UnsupportedEncodingException uee) {
			uee.printStackTrace();
		} catch (MalformedURLException mue) {
			mue.printStackTrace();
		}

		return url;
	}

	/**
	 * Convert an InputStream to String
	 * 
	 * @param is
	 *            The stream
	 * @return Stream as String
	 */
	private static String convertStreamToString(InputStream is) {
		try (Scanner s = new Scanner(is, "utf8")) {
			return s.useDelimiter("\\A").hasNext() ? s.next() : "";
		}
	}

}
