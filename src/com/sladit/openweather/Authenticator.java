package com.sladit.openweather;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * wetter.com openweather API authentication wrapper.
 * 
 * @author Leonid Koftun
 *
 */
public class Authenticator {
	private String projectName;
	private String apiKey;

	public Authenticator(String projectName, String apiKey) {
		this.projectName = projectName;
		this.apiKey = apiKey;
	}

	/**
	 * Generate MD5 checksum for API requests
	 * 
	 * @param query
	 * @return MD5 checksum
	 */
	String getChecksum(String query) {
		if (getProjectName() == null || apiKey == null) {
			return null;
		}
		String concat = getProjectName() + apiKey + query;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digest = md.digest(concat.getBytes());
			String hexDigest = convertByteArrayToHexString(digest);

			return hexDigest;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return null;
	}

	private static String convertByteArrayToHexString(byte[] arrayBytes) {
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < arrayBytes.length; i++) {
			stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
					.substring(1));
		}
		return stringBuffer.toString();
	}

	public String getProjectName() {
		return projectName;
	}
}
