#  Java client library for the [wetter.com](http://www.wetter.com) openweather API #

##Why wetter.com##
I had to make use of wetter.com data in a private Java project, so I decided I could as well write a little SDK as exercise. Now that it's there, I'm sharing it, just in case.

I'm not even sure there is demand for a SDK to wetter.com's API since there are better API alternatives available.
The wetter.com API is not documented so well (German only), does not offer that much data and doesn't seem to have full international coverage. The output language can be set to English, German and Spanish though, so there is some support out of the box.

From what I saw their API does have decently precise weather forecasts and I find that it can be used well for quick weather lookups.

##Live Demo##
I wrote a Demo Spring WebApp, which can be visited [here](http://openweather.sladit.com). 

[Accompanying source here.](https://bitbucket.org/sovcon/openweather-spring-example/overview)

##Prerequisites##
You'll need to have a project set up at [wetter.com](http://www.wetter.com/apps_und_mehr/website/api/projekte/) to receive an API key. (takes 1 min)


##Dependencies##
I chose the [google gson](https://code.google.com/p/google-gson/) library for json deserialization, so you must have that in your classpath.
##Usage##
Simply create an instance of ``OpenweatherConnection`` and call the main methods:

* ``queryLocation(String query)`` to find location results for a String query and save corresponding city codes that work as unique identifiers and are needed for 
* ``queryForecast(String cityCode)``.which will give you a ``Forecast`` that contains the following:

    * Up to three (limited by API) ``ForecastDay``s, where each object holds the daily average weather forecast and
    * Multiple ``ForecastTime``s, where each object holds the weather forecast information for a time on that day 

##Example##

```
#!java

// Exceptions are ignored in this example

String projectName = "your_project_name";
String apiKey = "your_api_key";

OpenweatherConnection connection = new OpenweatherConnection(projectName, apiKey); 

String location = "Munich";

LocationResponse response= connection.queryLocation(location);

System.out.println("Found " + response.getResults().size() + " results for search: '" + location + "'.");

// Loop over results that were found for our location search and print some weather data
for (LocationResult result : response.getResults()) {
    String cityCode = result.getCityCode(); // These codes identify a location, store them somewhere if you need persistence 

    ForecastResponse response2= connection.queryForecast(cityCode);

    ForecastDay today = response2.getCity().getForecast().getDays().get(0);

    System.out.println("Today in " + result.getName() + ", the weather will be: " + today.getWeatherStatusText());
    System.out.println("At a max. Temperature of " + today.getMaxTemp() + " celsius \n");

}

if (response.getResults().size() == 0) {
    System.out.println("No locations were found...");
}
		
```